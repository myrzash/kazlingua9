import 'package:flutter/material.dart';
import 'package:kazlingua9/models/MainModel.dart';
import 'package:kazlingua9/pages/AboutPage.dart';
import 'package:kazlingua9/pages/GamePage.dart';
import 'package:kazlingua9/pages/MainPage.dart';
import 'package:kazlingua9/pages/PartPage.dart';
import 'package:scoped_model/scoped_model.dart';

void main() => runApp(
      new ScopedModel<MainModel>(
        model: MainModel(),
        child: MaterialApp(
//      initialRoute: '/game',
          debugShowCheckedModeBanner: false,
          routes: {
            '/': (context) => MainPage(),
            '/about': (context) => AboutPage(),
            '/part': (context) => PartPage(),
            '/game': (context) => GamePage(),
          },
        ),
      ),
    );
