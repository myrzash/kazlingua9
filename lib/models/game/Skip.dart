class Skip {
  List list;

  Skip(this.list);

  Skip.fromJSON(json) {
    List list = json;
    this.list = list == null
        ? []
        : list
            .map((item) => item is Map
                ? SkipItem(item['skip'].toString().trim())
                : item.toString().trim().replaceAll(new RegExp(r"\s+"), " "))
            .toList();
  }
}

class SkipItem {
  String skip;

  SkipItem(this.skip);
}
