import 'package:flutter/material.dart';

class RoundedButton extends StatelessWidget {
  final String text;
  final Function onPressed;

  RoundedButton({this.text, this.onPressed});

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.symmetric(horizontal: 30, vertical: 10),
      child: ElevatedButton(
          child: Text(text,
              style: TextStyle(
                color: Colors.white,
                letterSpacing: 1,
                fontSize: 18,
                fontWeight: FontWeight.w600,
              )),
          onPressed: onPressed,
          style: ElevatedButton.styleFrom(
            primary: const Color(0xFF009ed6),
            padding: EdgeInsets.symmetric(vertical: 10, horizontal: 30),
            shape: RoundedRectangleBorder(
                borderRadius: new BorderRadius.circular(30.0)),
          )),
    );
  }
}
