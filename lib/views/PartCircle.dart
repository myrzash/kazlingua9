import 'package:flutter/material.dart';

class PartCircle extends StatelessWidget {
  final bool isFull;
  PartCircle({this.isFull = false});

  @override
  Widget build(BuildContext context) => new PhysicalModel(
        color: Colors.transparent,
        child: new Container(
          width: 8.0,
          height: 8.0,
          decoration: new BoxDecoration(
            color: isFull ? Colors.white : Colors.transparent,
            borderRadius: new BorderRadius.circular(10.0),
            border: new Border.all(
              width: 1.0,
              color: Colors.white,
            ),
          ),
        ),
      );
}
